#include <iostream>
#include <cmath>
#include "process.h"
#include "own_lib.h"

using namespace std;

/**
 * @brief 开始流程
 *
 * @param initX 初始x坐标
 * @param initY 初始y坐标
 * @param degree 旋转角度
 * @param length 生长长度
 * @param window 窗口对象
 */
void Process::startProcess(float initX, float initY, float degree, float length, sf::RenderWindow &window)
{
    // ***** 绘制分形树 *****
    // 递归结束条件: 生长长度小于1
    if (length < 1.0f)
        return;
    else
    {
        // 计算下一段生长长度
        float newlength = 0.67f * length;

        // 计算下一段生长的终点坐标
        float finalX = initX + (newlength * (float)cos(degree * (3.14 / 180.0f)));
        float finalY = initY + (newlength * (float)sin(degree * (3.14 / 180.0f)));

        // 绘制下一段生长线段
        sf::Vertex line[] = {
            sf::Vertex(sf::Vector2f(initX, initY)),
            sf::Vertex(sf::Vector2f(finalX, finalY)),
        };
        startProcess(finalX, finalY, degree + 60.0f, newlength, window);
        startProcess(finalX, finalY, degree - 30.0f, newlength, window);

        // 绘制生长线段
        window.draw(line, 2, sf::Lines);
        // line.setFillColor(sf::Color::Blue);
        // window.display(); // Uncomment for animation
    }
}

/**
 * @brief 运行流程
 *
 */
void Process::run()
{
    // ***** 运行流程 *****
    // 调用自定义库
    OwnLib::hello();
    // 创建窗口渲染对象
    sf::RenderWindow window(sf::VideoMode(1920, 1080), "Tree");
    // 清空窗口内容
    window.clear();
    // 开始绘制分形树
    startProcess(1000, 799, -90, 350, window);
    // 显示窗口内容
    window.display();

    // 判断是否关闭窗口
    sf::Event event;
    while (window.isOpen())
    {
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
    }
}