/**
 * @file process.h
 * @author yoruhaki (yoruhaki@outlook.com)
 * @brief This is the header file of the Process class.
 * @version 0.1
 * @date 2024-05-12
 *
 * @copyright Copyright (c) 2024
 *
 */
#pragma once
#include <SFML/Graphics.hpp>

/**
 * @brief 流程类
 *
 * @details 用来控制流程的类，包括开始流程、运行流程等功能。
 */
class Process
{
public:
    void startProcess(float initX, float initY, float degree, float length, sf::RenderWindow &window);
    void run();
};
