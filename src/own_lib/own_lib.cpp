#include <iostream>
#include "own_lib.h"

/**
 * @brief 打印 hello from OwnLib!
 *
 */
void OwnLib::hello()
{
    std::cout << "Hello from OwnLib!" << std::endl;
}