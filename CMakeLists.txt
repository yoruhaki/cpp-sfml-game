# cmake 版本限制
cmake_minimum_required(VERSION 3.10.0)

# 项目信息
project(sfml-game
    VERSION 0.1.0
    DESCRIPTION "A simple SFML game"
    HOMEPAGE_URL "https://gitee.com/yoruhaki/cpp-sfml-game"
    LANGUAGES CXX
)

# -------------- 变量设置 --------------
# CPP 版本
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")

# 编译输出文件夹路径
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

# 头文件路径
set(PROCESS_DIR ${CMAKE_SOURCE_DIR}/src/process)
set(OWN_LIB_DIR ${CMAKE_SOURCE_DIR}/src/own_lib)

# -------------- 第三方库设置 --------------

# 第三方库路径
set(EXTERNAL_DIR ${CMAKE_SOURCE_DIR}/external)

# SFML cmake 路径 (默认变量名称为 SFML_DIR，可以不使用 list 进行 dll 导入)
set(SFML_DIR ${EXTERNAL_DIR}/SFML-2.6.1/lib/cmake/SFML)

# list(APPEND CMAKE_MODULE_PATH ${EXTERNAL_DIR}/SFML-2.6.1/lib/cmake/SFML)

# 导入sfml的dll文件
file(GLOB_RECURSE PROJECT_DLL ${EXTERNAL_DIR}/*.dll)

# 导入 SFML 库
find_package(
    SFML 2.6
    COMPONENTS
    graphics window system network audio
    REQUIRED
)

# -------------- 项目文件导入 --------------
# 添加子目录
add_subdirectory(src)
