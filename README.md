# SFML Game Template - CPP 多媒体 SFML 游戏模板

## 内置第三方库 Third-party Libraries

- [SFML](https://www.sfml-dev.org/) - Simple and Fast Multimedia Library

## 许可证 License

- [MIT](https://github.com/sfml-game-template/cpp/blob/main/LICENSE) &copy; 2024 SFML Game Template

## 需要自行安装的库 Libraries to be installed manually

- [CMake](https://cmake.org/) - Cross-platform, open-source build system
- [MinGW-w64](http://mingw-w64.org/doku.php) - Minimalist GNU for Windows (32/64-bit)

## 目录结构 Folder Structure

### 临时目录结构 Temporary Folder Structure

具体可以参考当前仓库

#### 部分文件/文件夹的作用 Purpose of some files/folders

1. `CMakeLists.txt` - 项目的 CMake 配置文件，用于构建项目
2. `include` - 项目的头文件目录，包含游戏逻辑相关的头文件
3. `src` - 项目的源文件目录，包含游戏逻辑相关的源文件
4. `assets` - 项目的资源文件目录，包含游戏使用的图片、音频、字体等资源文件
5. `build` - 项目的构建目录，用于存放项目的中间文件
6. `bin` - 项目的可执行文件目录，用于存放项目的可执行文件
7. `docs` - 项目的文档目录，用于存放项目的文档文件
8. `external` - 项目的第三方库目录，用于存放项目依赖的第三方库
9. `.vscode` - 项目的 VSCode 配置目录，用于存放项目的 VSCode 配置文件
10. `.gitignore` - 项目的 Git 忽略文件，用于指定 Git 忽略哪些文件不提交到 Git 仓库
11. `README.md` - 项目的说明文档，用于介绍项目的基本信息
12. `LICENSE` - 项目的许可证文件，用于声明项目的使用许可证

---

其中，process 暂时用来实现主要程序流程，OwnLib 部分用来重构抽离出游戏对象和逻辑，
planning_main.cpp 为程序入口文件。目前以多动态库导入的方式来管理项目代码。
暂时用不到 include 文件夹，后续可能会以混合结构的方式来组织代码。

### 预期目录结构 Expected Folder Structure

```shell
.
├── CMakeLists.txt
├── include
│   ├── game.h
│   ├── game_config.h
│   ├── game_state.h
│   ├── game_window.h
│   ├── input.h
│   ├── resource.h
│   └── utils.h
├── src
│   ├── game.cpp
│   ├── game_config.cpp
│   ├── game_state.cpp
│   ├── game_window.cpp
│   ├── input.cpp
│   ├── main.cpp
│   ├── resource.cpp
│   ├── lib
│   │   └──  ...
│   ├── process
│   │   └──  ...
│   └── utils.cpp
├── assets
│   ├── images
│   ├── sounds
│   └── fonts
├── build
│   └──...
├── bin
│   └──...
├── docs
│   └── ...
├── external
│   └── SFML
│       └── ...
├── .vscode
│   └── ...
├── .gitignore
├── README.md
└── LICENSE
```
